<?php

namespace SONUser\Entity;

use Doctrine\ORM\EntityRepository;
use SONUser\Entity\User as UserEntity;

/**
 * Class UserRepository
 * @package SONUser\Entity
 */
class UserRepository extends EntityRepository
{
    /**
     * @param $email
     * @param $password
     * @return null|User
     */
    public function findByEmailAndPassword($email, $password)
    {
        /** @var UserEntity $user */
        $user = $this->findOneBy(['email' => $email]);
        if ($user && password_verify($password, $user->getPassword())) {
            return $user;
        }
        return null;
    }
}
