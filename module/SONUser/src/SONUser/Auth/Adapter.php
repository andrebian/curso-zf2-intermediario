<?php

namespace SONUser\Auth;

use Doctrine\ORM\EntityManager;
use SONUser\Entity\User as UserEntity;
use SONUser\Entity\UserRepository;
use Zend\Authentication\Adapter\AdapterInterface;
use Zend\Authentication\Result;

/**
 * Class Adapter
 * @package SONUser\Auth
 */
class Adapter implements AdapterInterface
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $password;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Adapter
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Adapter
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function authenticate()
    {
        /** @var UserRepository $repository */
        $repository = $this->entityManager->getRepository(UserEntity::class);
        $user = $repository->findByEmailAndPassword($this->getUsername(), $this->getPassword());

        if ($user) {
            return new Result(Result::SUCCESS, ['user' => $user], ['ok']);
        }
        return new Result(Result::FAILURE_CREDENTIAL_INVALID, null, ['fail']);
    }
}
