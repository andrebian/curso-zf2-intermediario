<?php

namespace SONUser\Form;

use Zend\Form\Element\Password;
use Zend\Form\Element\Text;
use Zend\Form\Form;

/**
 * Class Login
 * @package SONUser\Form
 */
class Login extends Form
{
    public function __construct($name = 'user', $options = [])
    {
        parent::__construct($name);

        $this->setAttribute('method', 'POST');
        $this->setAttribute('role', 'form');

        $email = new Text('email');
        $email->setLabel('Email: ')->setAttribute('placeholder', 'Entre com o email')
            ->setAttribute('class', 'form-control');
        $this->add($email);

        $password = new Password('password');
        $password->setLabel('Password: ')->setAttribute('placeholder', 'Entre com a senha')
            ->setAttribute('class', 'form-control');
        $this->add($password);

        $this->add([
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => [
                'value' => 'Autenticar',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}
