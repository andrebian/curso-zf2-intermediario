<?php

namespace SONUser\View\Helper;

use Zend\Authentication\AuthenticationService;
use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\Storage\Session as SessionStorage;

/**
 * Class UserItendity
 * @package SONUser\View\Helper
 */
class UserIdentity extends AbstractHelper
{
    /**
     * @var AuthenticationService
     */
    protected $authService;

    /**
     * @return AuthenticationService
     */
    public function getAuthService()
    {
        return $this->authService;
    }

    public function __invoke($namespace = null)
    {
        $sessionStorage = new SessionStorage('SONUser');
        $this->authService = new AuthenticationService($sessionStorage);

        if ($this->getAuthService()->hasIdentity()) {
            return $this->getAuthService()->getIdentity();
        }

        return false;
    }
}
