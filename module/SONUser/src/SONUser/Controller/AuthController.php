<?php

namespace SONUser\Controller;

use SONUser\Auth\Adapter as AuthAdapter;
use SONUser\Form\Login;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * Class AuthController
 * @package SONUser\Controller
 */
class AuthController extends AbstractActionController
{
    /**
     * @return \Zend\Http\Response|ViewModel
     */
    public function indexAction()
    {
        $form = new Login();
        $request = $this->getRequest();
        $error = false;

        if ($request->isPost()) {
            $form->setData($request->getPost());
            if ($form->isValid()) {
                $data = $request->getPost()->toArray();

                $sessionStorage = new SessionStorage('SONUser');
                $auth = new AuthenticationService($sessionStorage);

                $authAdapter = $this->serviceLocator->get(AuthAdapter::class);
                $authAdapter->setUsername($data['email']);
                $authAdapter->setPassword($data['password']);

                $result = $auth->authenticate($authAdapter);
                if ($result->isValid()) {
                    $sessionStorage->write($auth->getIdentity()['user']);
                    return $this->redirect()->toRoute('sonuser-admin');
                } else {
                    $error = true;
                }
            }
        }

        return new ViewModel([
            'form' => $form,
            'error' => $error
        ]);
    }

    /**
     * @return \Zend\Http\Response
     */
    public function logoutAction()
    {
        $sessionStorage = new SessionStorage('SONUser');
        $auth = new AuthenticationService($sessionStorage);
        $auth->clearIdentity();
        return $this->redirect()->toRoute('sonuser-auth');
    }
}
