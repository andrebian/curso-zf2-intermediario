<?php

namespace SONUser;

use Doctrine\ORM\EntityManager;
use SONUser\Auth\Adapter as AuthAdapter;
use SONUser\View\Helper\UserIdentity;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;
use Zend\ModuleManager\ModuleManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Controller\AbstractController;
use Zend\Mvc\MvcEvent;

/**
 * Class Module
 * @package SONBase
 */
class Module
{
    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    /**
     * @param ModuleManager $moduleManager
     */
    public function init(ModuleManager $moduleManager)
    {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();

        $sharedEvents->attach(
            AbstractActionController::class,
            MvcEvent::EVENT_DISPATCH,
            [$this, 'validateAuth'],
            100
        );
    }

    /**
     * @param MvcEvent $event
     * @return \Zend\Http\Response
     */
    public function validateAuth(MvcEvent $event)
    {
        $sessionStorage = new SessionStorage('SONUser');
        $auth = new AuthenticationService($sessionStorage);


        /** @var AbstractController $controller */
        $controller = $event->getTarget();
        $matchedRoute = $controller->getEvent()->getRouteMatch()->getMatchedRouteName();

        $whiteList = [
            'sonuser-register',
            'sonuser-activate',
            'sonuser-auth'
        ];

        if (!$auth->hasIdentity() && !in_array($matchedRoute, $whiteList)) {
            return $controller->redirect()->toRoute('sonuser-auth');
        }
    }

    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'SONUser\Mail\Transport' => function ($serviceManager) {
                    $config = $serviceManager->get('config');

                    $transport = new SmtpTransport();
                    $options = new SmtpOptions($config['mail']);
                    $transport->setOptions($options);

                    return $transport;
                },
                'SONUser\Service\User' => function ($serviceManager) {
                    return new Service\User(
                        $serviceManager->get(EntityManager::class),
                        $serviceManager->get('SONUser\Mail\Transport'),
                        $serviceManager->get('View')
                    );
                },
                AuthAdapter::class => function ($serviceManager) {
                    $entityManager = $serviceManager->get(EntityManager::class);
                    return new AuthAdapter($entityManager);
                }
            ],
        ];
    }

    public function getViewHelperConfig()
    {
        return [
            'invokables' => [
                'userIdentity' => UserIdentity::class
            ]
        ];
    }

}
