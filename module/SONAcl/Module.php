<?php

namespace SONAcl;

use Doctrine\ORM\EntityManager;
use SONAcl\Entity\Privilege as PrivilegeEntity;
use SONAcl\Entity\PrivilegeRepository;
use SONAcl\Entity\Resource as ResourceEntity;
use SONAcl\Entity\ResourceRepository;
use SONAcl\Entity\Role as RoleEntity;
use SONAcl\Entity\RoleRepository;
use SONAcl\Form\Privilege as PrivilegeForm;
use SONAcl\Form\Role as RoleForm;
use SONAcl\Permissions\Acl;
use SONAcl\Service\Privilege;
use SONAcl\Service\Resource;
use SONAcl\Service\Role;
use Zend\ServiceManager\ServiceManager;

/**
 * Class Module
 * @package SONAcl
 */
class Module
{
    /**
     * @return mixed
     */
    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    /**
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }


    /**
     * @return array
     */
    public function getServiceConfig()
    {
        return [
            'factories' => [

                // Services
                Role::class => function ($serviceManager) {
                    return new Role($serviceManager->get(EntityManager::class));
                },
                Resource::class => function ($serviceManager) {
                    return new Resource($serviceManager->get(EntityManager::class));
                },
                Privilege::class => function ($serviceManager) {
                    return new Privilege($serviceManager->get(EntityManager::class));
                },

                // Forms
                RoleForm::class => function (ServiceManager $serviceManager) {
                    /** @var EntityManager $entityManager */
                    $entityManager = $serviceManager->get(EntityManager::class);
                    /** @var RoleRepository $repo */
                    $repo = $entityManager->getRepository(RoleEntity::class);
                    $parent = $repo->fetchParent();

                    return new RoleForm('role', $parent);
                },
                PrivilegeForm::class => function (ServiceManager $serviceManager) {
                    /** @var EntityManager $entityManager */
                    $entityManager = $serviceManager->get(EntityManager::class);

                    /** @var RoleRepository $repoParent */
                    $repoParent = $entityManager->getRepository(RoleEntity::class);
                    $parent = $repoParent->fetchParent();

                    /** @var ResourceRepository $repoResource */
                    $repoResource = $entityManager->getRepository(ResourceEntity::class);
                    $resources = $repoResource->fetchPairs();

                    return new PrivilegeForm('privilege', $parent, $resources);
                },

                // ACL
                Acl::class => function ($serviceManager) {
                    $entityManager = $serviceManager->get(EntityManager::class);
                    $roles = $entityManager->getRepository(RoleEntity::class)->findAll();
                    $resources = $entityManager->getRepository(ResourceEntity::class)->findAll();
                    $privileges = $entityManager->getRepository(PrivilegeEntity::class)->findAll();

                    return new Acl($roles, $resources, $privileges);
                },
            ]
        ];
    }
}
