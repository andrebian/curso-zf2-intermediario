<?php

namespace SONAcl;

use SONAcl\Controller\PrivilegesController;
use SONAcl\Controller\ResourcesController;
use SONAcl\Controller\RolesController;
use Zend\Mvc\Router\Http\Literal;
use Zend\Mvc\Router\Http\Segment;

return [
    'router' => [
        'routes' => [
            'sonacl-admin' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/admin/acl',
                    'defaults' => [
                        'controller' => RolesController::class,
                        'action' => 'index'
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'roles' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/roles[/:action[/:id]]',
                            'defaults' => [
                                'controller' => RolesController::class
                            ]
                        ]
                    ],
                    'resources' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/resources[/:action[/:id]]',
                            'defaults' => [
                                'controller' => ResourcesController::class
                            ]
                        ]
                    ],
                    'privileges' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/privileges[/:action[/:id]]',
                            'defaults' => [
                                'controller' => PrivilegesController::class
                            ]
                        ]
                    ],
                ]
            ],
        ]
    ],
    'controllers' => [
        'invokables' => [
            RolesController::class => RolesController::class,
            ResourcesController::class => ResourcesController::class,
            PrivilegesController::class => PrivilegesController::class
        ]
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'doctrine' => [
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'cache' => 'array',
                'paths' => [dirname(__DIR__) . '/src/' . __NAMESPACE__ . '/Entity']
            ],
            'orm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver'
                ]
            ]
        ],
    ],
    'data-fixture' => [
        __NAMESPACE__ . '_fixture' => __DIR__ . '/../src/' . __NAMESPACE__ . '/Fixture',
    ],
];
