<?php

namespace SONAcl\Permissions;

use SONAcl\Entity\Privilege as PrivilegeEntity;
use SONAcl\Entity\Resource as ResourceEntity;
use SONAcl\Entity\Role as RoleEntity;
use Zend\Permissions\Acl\Acl as BaseAcl;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;
use Zend\Permissions\Acl\Role\GenericRole as Role;

/**
 * Class ACL
 * @package SONAcl\Permissions
 */
class Acl extends BaseAcl
{
    /**
     * @var array
     */
    protected $rolesCollection;

    /**
     * @var array
     */
    protected $resourcesCollection;

    /**
     * @var array
     */
    protected $privilegesCollection;

    /**
     * @param array $rolesCollection
     * @param array $resourcesCollection
     * @param array $privilegesCollection
     */
    public function __construct(array $rolesCollection, array $resourcesCollection, array $privilegesCollection)
    {
        $this->rolesCollection = $rolesCollection;
        $this->resourcesCollection = $resourcesCollection;
        $this->privilegesCollection = $privilegesCollection;

        $this->loadRoles();
        $this->loadResources();
        $this->loadPrivileges();
    }

    /**
     * Register all resources
     */
    protected function loadResources()
    {
        /** @var ResourceEntity $resource */
        foreach ($this->resourcesCollection as $resource) {
            $this->addResource(new Resource($resource->getName()));
        }
    }

    /**
     * Register all roles
     */
    protected function loadRoles()
    {
        /** @var RoleEntity $role */
        foreach ($this->rolesCollection as $role) {

            if ($role->getParent()) {
                $this->addRole(new Role($role->getName()), new Role($role->getParent()->getName()));
            } else {
                $this->addRole(new Role($role->getName()));
            }

            if ($role->isIsAdmin()) {
                $this->allow($role->getName(), [], []);
            }
        }
    }

    /**
     * Register all privileges
     */
    protected function loadPrivileges()
    {
        /** @var PrivilegeEntity $privilege */
        foreach ($this->privilegesCollection as $privilege) {
            $this->allow($privilege->getRole()->getName(), $privilege->getResource()->getName(), $privilege->getName());
        }
    }
}
