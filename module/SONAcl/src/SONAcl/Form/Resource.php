<?php

namespace SONAcl\Form;

use Zend\Form\Element\Csrf;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Text;
use Zend\Form\Form;

/**
 * Class Resource
 * @package SONAcl\Form
 */
class Resource extends Form
{
    public function __construct($name = 'role')
    {
        parent::__construct($name);

        $this->setAttribute('method', 'POST');
        $this->setAttribute('role', 'form');

        $id = new Hidden('id');
        $this->add($id);

        $name = new Text('name');
        $name->setLabel('Nome: ')->setAttribute('placeholder', 'Entre com o nome')
            ->setAttribute('class', 'form-control');
        $this->add($name);

        $csrf = new Csrf('security');
        $this->add($csrf);

        $this->add([
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => [
                'value' => 'Salvar',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}
