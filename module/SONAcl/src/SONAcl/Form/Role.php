<?php

namespace SONAcl\Form;

use Zend\Form\Element\Checkbox;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Form;

/**
 * Class Role
 * @package SONAcl\Form
 */
class Role extends Form
{
    protected $parent;

    public function __construct($name = 'role', $parent = [])
    {
        parent::__construct($name);

        $this->parent = $parent;
        $this->setAttribute('method', 'POST');
        $this->setAttribute('role', 'form');

        $id = new Hidden('id');
        $this->add($id);

        $name = new Text('name');
        $name->setLabel('Nome: ')->setAttribute('placeholder', 'Entre com o nome')
            ->setAttribute('class', 'form-control');
        $this->add($name);

        $allOptions = [0 => 'Sem herança'] + $this->parent;

        $parent = new Select();
        $parent->setLabel('Herda: ')
            ->setName('parent')
            ->setValueOptions($allOptions)
            ->setAttributes([
                'class' => 'form-control'
            ]);
        $this->add($parent);

        $isAdmin = new Checkbox();
        $isAdmin->setName('isAdmin');
        $isAdmin->setLabel('Admin?: ')
            ->setAttributes([
                'class' => 'form-control'
            ]);
        $this->add($isAdmin);

        $csrf = new Csrf('security');
        $this->add($csrf);

        $this->add([
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => [
                'value' => 'Salvar',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}
