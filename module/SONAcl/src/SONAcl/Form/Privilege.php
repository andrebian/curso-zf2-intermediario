<?php

namespace SONAcl\Form;

use Zend\Form\Element\Csrf;
use Zend\Form\Element\Hidden;
use Zend\Form\Element\Select;
use Zend\Form\Element\Text;
use Zend\Form\Form;

/**
 * Class Privilege
 * @package SONAcl\Form
 */
class Privilege extends Form
{
    /**
     * @var array
     */
    protected $roles;

    /**
     * @var array
     */
    protected $resources;

    public function __construct($name = 'role', $roles, $resources)
    {
        parent::__construct($name);

        $this->roles = $roles;
        $this->resources = $resources;
        $this->setAttribute('method', 'POST');
        $this->setAttribute('role', 'form');

        $id = new Hidden('id');
        $this->add($id);

        $name = new Text('name');
        $name->setLabel('Nome: ')->setAttribute('placeholder', 'Entre com o nome')
            ->setAttribute('class', 'form-control');
        $this->add($name);

        $role = new Select();
        $role->setLabel('Role: ')
            ->setName('role')
            ->setOptions(['value_options' => $this->roles])
            ->setAttribute('class', 'form-control');
        $this->add($role);

        $resource = new Select();
        $resource->setLabel('Resource: ')
            ->setName('resource')
            ->setOptions(['value_options' => $this->resources])
            ->setAttribute('class', 'form-control');
        $this->add($resource);

        $csrf = new Csrf('security');
        $this->add($csrf);

        $this->add([
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => [
                'value' => 'Salvar',
                'class' => 'btn btn-success'
            ]
        ]);
    }
}
