<?php

namespace SONAcl\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class RoleRepository
 * @package SONAcl\Entity
 */
class RoleRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function fetchParent()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            $array[$entity->getId()] = $entity->getName();
        }
        return $array;
    }
}
