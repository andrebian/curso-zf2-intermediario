<?php

namespace SONAcl\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class PrivilegeRepository
 * @package SONAcl\Entity
 */
class PrivilegeRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function fetchPairs()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            $array[$entity->getId()] = $entity->getName();
        }

        return $array;
    }
}
