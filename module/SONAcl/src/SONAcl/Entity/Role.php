<?php

namespace SONAcl\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Zend\Hydrator\ClassMethods;

/**
 * Class Role
 * @package SONAcl\Entity
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="sonacl_roles")
 * @ORM\Entity(repositoryClass="SONAcl\Entity\RoleRepository")
 */
class Role
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Role
     * @ORM\OneToOne(targetEntity="SONAcl\Entity\Role")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var string
     * @ORM\Column(type="text")
     */
    private $name;

    /**
     * @var bool
     * @ORM\Column(type="boolean", name="is_admin")
     */
    private $isAdmin;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     * @return Role
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Role
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param boolean $isAdmin
     * @return Role
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param $created
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @ORM\PrePersist
     * @return $this
     */
    public function setUpdated()
    {
        $this->updated = new DateTime();
        return $this;
    }

    public function __construct($options = [])
    {
        $this->created = new DateTime();
        $this->updated = new DateTime();

        if (!empty($options)) {
            $hydrator = new ClassMethods(false);
            $hydrator->hydrate($options, $this);
        }
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $parent = false;
        if (isset($this->parent)) {
            $parent = $this->parent->getId();
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'isAdmin' => $this->isAdmin,
            'parent' => $parent
        ];
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }
}
