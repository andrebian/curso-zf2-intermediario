<?php

namespace SONAcl\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * Class ResourceRepository
 * @package SONAcl\Entity
 */
class ResourceRepository extends EntityRepository
{
    /**
     * @return array
     */
    public function fetchPairs()
    {
        $entities = $this->findAll();
        $array = [];

        foreach ($entities as $entity) {
            $array[$entity->getId()] = $entity->getName();
        }

        return $array;
    }
}
