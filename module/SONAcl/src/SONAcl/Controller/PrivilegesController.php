<?php

namespace SONAcl\Controller;

use SONAcl\Entity\Privilege as PrivilegeEntity;
use SONAcl\Form\Privilege as PrivilegeForm;
use SONAcl\Service\Privilege as PrivilegeService;
use SONBase\Controller\CrudController;
use SONBase\Service\ServiceInterface;
use SONUser\Entity\EntityInterface;
use Zend\View\Model\ViewModel;

/**
 * Class PrivilegesController
 * @package SONAcl\Controller
 */
class PrivilegesController extends CrudController
{

    public function __construct()
    {
        $this->service = PrivilegeService::class;
        $this->entity = PrivilegeEntity::class;
        $this->form = PrivilegeForm::class;
        $this->controller = 'privileges';
        $this->route = 'sonacl-admin/privileges';
    }

    /**
     * @return \Zend\Http\Response
     */
    public function newAction()
    {
        $form = $this->serviceLocator->get($this->form);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $form->setData($data);

            if ($form->isValid()) {
                $service = $this->serviceLocator->get($this->service);
                $service->insert($data);
                return $this->redirect()->toRoute($this->route, ['controller' => $this->controller]);
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * @return \Zend\Http\Response
     */
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $form = $this->serviceLocator->get(PrivilegeForm::class);
        $request = $this->getRequest();

        /** @var EntityInterface $entity */
        $entity = $this->getEntityManager()->find($this->entity, $id);
        if ($entity) {
            $form->setData($entity->toArray());
        }

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                /** @var ServiceInterface $service */
                $service = $this->serviceLocator->get($this->service);
                $service->update($request->getPost()->toArray());
                return $this->redirect()->toRoute($this->route, ['controller' => $this->controller]);
            }
        }

        return new ViewModel([
            'form' => $form,
            'id' => $id
        ]);
    }
}
