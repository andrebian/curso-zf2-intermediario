<?php

namespace SONAcl\Controller;

use SONAcl\Entity\Role as RoleEntity;
use SONAcl\Form\Role as RoleForm;
use SONAcl\Permissions\Acl;
use SONAcl\Service\Role as RoleService;
use SONBase\Controller\CrudController;
use SONBase\Service\ServiceInterface;
use SONUser\Entity\EntityInterface;
use Zend\View\Model\ViewModel;

/**
 * Class RolesController
 * @package SONAcl\Controller
 */
class RolesController extends CrudController
{

    public function __construct()
    {
        $this->service = RoleService::class;
        $this->entity = RoleEntity::class;
        $this->form = RoleForm::class;
        $this->controller = 'roles';
        $this->route = 'sonacl-admin';
    }

    /**
     * @return \Zend\Http\Response
     */
    public function newAction()
    {
        $form = $this->serviceLocator->get($this->form);
        $request = $this->getRequest();

        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $form->setData($data);

            if ($form->isValid()) {
                $service = $this->serviceLocator->get($this->service);
                $service->insert($data);
                return $this->redirect()->toRoute($this->route, ['controller' => $this->controller]);
            }
        }

        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * @return \Zend\Http\Response
     */
    public function editAction()
    {
        $id = $this->params()->fromRoute('id');
        $form = $this->serviceLocator->get(RoleForm::class);
        $request = $this->getRequest();

        /** @var EntityInterface $entity */
        $entity = $this->getEntityManager()->find($this->entity, $id);
        if ($entity) {
            $form->setData($entity->toArray());
        }

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {
                /** @var ServiceInterface $service */
                $service = $this->serviceLocator->get($this->service);
                $service->update($request->getPost()->toArray());
                return $this->redirect()->toRoute($this->route, ['controller' => $this->controller]);
            }
        }

        return new ViewModel([
            'form' => $form,
            'id' => $id
        ]);
    }

    public function testAction()
    {
        /** @var Acl $acl */
        $acl = $this->serviceLocator->get(Acl::class);

        echo $acl->isAllowed('Teste', 'Customer\IndexController', 'edit') ? 'Permitido' : 'Negado';
        die();
    }
}
