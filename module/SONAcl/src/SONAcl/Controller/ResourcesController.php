<?php

namespace SONAcl\Controller;

use SONAcl\Entity\Resource as ResourceEntity;
use SONAcl\Form\Resource as ResourceForm;
use SONAcl\Service\Resource as ResourceService;
use SONBase\Controller\CrudController;

/**
 * Class RolesController
 * @package SONAcl\Controller
 */
class ResourcesController extends CrudController
{

    public function __construct()
    {
        $this->service = ResourceService::class;
        $this->entity = ResourceEntity::class;
        $this->form = ResourceForm::class;
        $this->controller = 'resources';
        $this->route = 'sonacl-admin/resources';
    }
}
