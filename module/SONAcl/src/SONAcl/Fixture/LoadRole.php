<?php

namespace SONAcl\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SONAcl\Entity\Role;

/**
 * Class LoadRole
 * @package SONAcl\Fixture
 */
class LoadRole extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    function load(ObjectManager $manager)
    {
        print(" - Importing data fixture to Role .");

        $visitor = new Role([
            'name' => 'Usuário visitante',
            'isAdmin' => false
        ]);
        $manager->persist($visitor);


        $registered = new Role([
            'name' => 'Usuário registrado',
            'isAdmin' => false,
            'parent' => $visitor
        ]);
        $manager->persist($registered);

        $editor = new Role([
            'name' => 'Editor',
            'isAdmin' => false,
            'parent' => $registered
        ]);
        $manager->persist($editor);

        $admin = new Role([
            'name' => 'Admin',
            'isAdmin' => true,
        ]);
        $manager->persist($admin);

        $manager->flush();

        print(".");
        print(".");
        print(" Ok \r\n");
    }

    /**
     * @inheritdoc
     */
    function getOrder()
    {
        return 1;
    }
}
