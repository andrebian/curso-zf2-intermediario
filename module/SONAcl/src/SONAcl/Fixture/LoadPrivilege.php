<?php

namespace SONAcl\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SONAcl\Entity\Privilege;
use SONAcl\Entity\Resource;
use SONAcl\Entity\Role;

/**
 * Class LoadPrivilege
 * @package SONAcl\Fixture
 */
class LoadPrivilege extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    function load(ObjectManager $manager)
    {
        print(" - Importing data fixture to Privileges .");

        $role1 = $manager->getReference(Role::class, 1);
        $resource1 = $manager->getReference(Resource::class, 1);

        $role2 = $manager->getReference(Role::class, 2);
        $resource2 = $manager->getReference(Resource::class, 2);

        $role3 = $manager->getReference(Role::class, 3);
        $resource3 = $manager->getReference(Resource::class, 3);

        $role4 = $manager->getReference(Role::class, 4);
        $resource4 = $manager->getReference(Resource::class, 4);

        $privilege = new Privilege([
            'name' => 'Visualizar',
            'role' => $role1,
            'resource' => $resource1
        ]);
        $manager->persist($privilege);

        $privilege = new Privilege([
            'name' => 'Novo / Editar',
            'role' => $role3,
            'resource' => $resource1
        ]);
        $manager->persist($privilege);

        $privilege = new Privilege([
            'name' => 'Excluir',
            'role' => $role4,
            'resource' => $resource1
        ]);
        $manager->persist($privilege);

        $manager->flush();

        print(".");
        print(".");
        print(" Ok \r\n");
    }

    /**
     * @inheritdoc
     */
    function getOrder()
    {
        return 3;
    }
}
