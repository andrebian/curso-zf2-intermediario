<?php

namespace SONAcl\Fixture;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use SONAcl\Entity\Resource;

/**
 * Class LoadResource
 * @package SONAcl\Fixture
 */
class LoadResource extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    function load(ObjectManager $manager)
    {
        print(" - Importing data fixture to Resource .");

        $posts = new Resource([
            'name' => 'Posts'
        ]);

        $pages = new Resource([
            'name' => 'Pages'
        ]);

        $manager->persist($posts);
        $manager->persist($pages);

        $manager->flush();

        print(".");
        print(".");
        print(" Ok \r\n");
    }

    /**
     * @inheritdoc
     */
    function getOrder()
    {
        return 2;
    }
}
