<?php

namespace SONAcl\Service;

use Doctrine\ORM\EntityManager;
use SONAcl\Entity\Privilege as PrivilegeEntity;
use SONAcl\Entity\Resource as ResourceEntity;
use SONAcl\Entity\Role as RoleEntity;
use SONBase\Service\AbstractService;
use Zend\Hydrator\ClassMethods;

/**
 * Class Privilege
 * @package SONAcl\Service
 */
class Privilege extends AbstractService
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entity = PrivilegeEntity::class;
    }

    /**
     * @inheritdoc
     */
    public function insert(array $data)
    {
        $entity = new $this->entity($data);

        $role = $this->entityManager->getReference(RoleEntity::class, (int)$data['role']);
        $entity->setRole($role);

        $resource = $this->entityManager->getReference(ResourceEntity::class, (int)$data['resource']);
        $entity->setResource($resource);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }

    /**
     * @inheritdoc
     */
    public function update(array $data)
    {
        $entity = $this->entityManager->getReference($this->entity, $data['id']);
        (new ClassMethods())->hydrate($data, $entity);

        $role = $this->entityManager->getReference(RoleEntity::class, (int)$data['role']);
        $entity->setRole($role);

        $resource = $this->entityManager->getReference(ResourceEntity::class, (int)$data['resource']);
        $entity->setResource($resource);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }
}
