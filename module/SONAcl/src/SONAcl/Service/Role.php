<?php

namespace SONAcl\Service;

use Doctrine\ORM\EntityManager;
use SONBase\Service\AbstractService;
use SONAcl\Entity\Role as RoleEntity;
use Zend\Hydrator\ClassMethods;

/**
 * Class Role
 * @package SONAcl\Service
 */
class Role extends AbstractService
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entity = RoleEntity::class;
    }

    public function insert(array $data)
    {
        if (isset($data['parent']) && 0 != $data['parent']) {
            $parent = $this->entityManager->getReference($this->entity, (int)$data['parent']);
            $data['parent'] = $parent;
        } else {
            unset($data['parent']);
        }

        $entity = new $this->entity($data);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }

    public function update(array $data)
    {
        $entity = $this->entityManager->getReference($this->entity, $data['id']);
        if (isset($data['parent'])) {
            $parent = $this->entityManager->getReference($this->entity, (int)$data['parent']);
            $data['parent'] = $parent;
        }

        (new ClassMethods())->hydrate($data, $entity);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $entity;
    }
}
