<?php

namespace SONAcl\Service;

use Doctrine\ORM\EntityManager;
use SONAcl\Entity\Resource as ResourceEntity;
use SONBase\Service\AbstractService;

/**
 * Class Resource
 * @package SONAcl\Service
 */
class Resource extends AbstractService
{
    public function __construct(EntityManager $entityManager)
    {
        parent::__construct($entityManager);
        $this->entity = ResourceEntity::class;
    }
}
